package ba.izvornade.izvornade;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ApplyActivity extends AppCompatActivity {

    Button button;
    StringBuilder emailBody;
    EditText imePrezime, godiste, adresa, skola, zelimSkolu;
    String uspijeh, spol, zelimBiti, vladanje;
    CheckBox muško;
    CheckBox žensko;


    //RadioButtons text
    TextView nivoObrazovanjaText;
    TextView spolText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply);

        //Back button
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        button = (Button) findViewById(R.id.apply);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {








                sendEmail();
            }
        });


        createDroppingList ();
        createDroppingList2();
        checkBoxes();
        checkBoxes2();


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendEmail() {

        final String subject = "Aplikacija za smještaj";


        //EditTexts
        imePrezime = (EditText) findViewById(R.id.imePrezime);
        String nameSurname = imePrezime.getText().toString();
        godiste = (EditText) findViewById(R.id.godiste);
        String birthYear = godiste.getText().toString();
        adresa = (EditText) findViewById(R.id.adresa);
        String adress = adresa.getText().toString();
        skola = (EditText) findViewById(R.id.skola);
        String school = skola.getText().toString();
        zelimSkolu = (EditText) findViewById(R.id.zelimSkolu);
        String wantedSchool = zelimSkolu.getText().toString();




        //radioButtons text
        nivoObrazovanjaText = (TextView) findViewById(R.id.nivoObrazovanja);
        spolText = (TextView) findViewById(R.id.spol);

        //radioButtons
        RadioButton male = (RadioButton) findViewById(R.id.muško);
        RadioButton female = (RadioButton) findViewById(R.id.žensko);
        RadioButton srednjac = (RadioButton) findViewById(R.id.srednjoskolac);
        RadioButton fakultet = (RadioButton) findViewById(R.id.student);





        //email body string
        emailBody = new StringBuilder();
        emailBody.append("Poštovani,\n Ja sam ");
        emailBody.append(nameSurname);
        emailBody.append("(" + spol + ")");
        emailBody.append(", rođen sam ");
        emailBody.append(birthYear);
        emailBody.append(" u ");
        emailBody.append(adress + "-u. ");
        emailBody.append("\nOvom prilikom želim da apliciram za smještaj u Fondaciji Izvor nade u Sarajevu kao " + zelimBiti + "." );
        emailBody.append("\nS ponosom želim da Vam kažem da sam svoje obrazovanje završio sa ");
        emailBody.append(uspijeh);
        emailBody.append(" uspijehom, što smatram veoma važnim faktorom za moje dalje školovanje.");
        emailBody.append("\nZavršio sam " + school + ", sa "  + vladanje + " vladanjem.");
        emailBody.append("\nSvoje školovanje želim da nastavim u/na " + wantedSchool + " školi/fakultetu");
        emailBody.append("\nS poštovanjem\n" + nameSurname);


        //email intent
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
        intent.setType("message/rfc822");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@izvornade.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, emailBody.toString());

        if (imePrezime.getText().toString().equalsIgnoreCase("") ||
                godiste.getText().toString().equalsIgnoreCase("") ||
                adresa.getText().toString().equalsIgnoreCase("") ||
                skola.getText().toString().equalsIgnoreCase("") ||
                zelimSkolu.getText().toString().equalsIgnoreCase("") || (
                !male.isChecked() && !female.isChecked()) ||
                (!srednjac.isChecked() && !fakultet.isChecked()) ||
                !birthYear.matches("\\d{2}.\\d{2}.\\d{4}")
                ) {


                if (imePrezime.getText().toString().equalsIgnoreCase("") ||
                        godiste.getText().toString().equalsIgnoreCase("") ||
                        adresa.getText().toString().equalsIgnoreCase("") ||
                        skola.getText().toString().equalsIgnoreCase("") ||
                        zelimSkolu.getText().toString().equalsIgnoreCase("") || (
                        !male.isChecked() && !female.isChecked()) ||
                        (!srednjac.isChecked() && !fakultet.isChecked())) {
                    checkTheInput(imePrezime, godiste, adresa, skola, zelimSkolu, nivoObrazovanjaText, spolText, male, female, srednjac, fakultet);
                    Toast toast1 = Toast.makeText(this, "Fill out all the fields", 200);
                    toast1.show();

                }
            else if(!birthYear.matches("\\d{2}.\\d{2}.\\d{4}")) {
                Toast toast = Toast.makeText(this, "Unesi slijedeći format za godište:\nDD.MM.GGGG", 200);
                toast.show();

            }else {
                Toast toast = Toast.makeText(this, "Fill out all the fields", 200);
                toast.show();

            }








        } else{
            try {
                startActivity(Intent.createChooser(intent, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(ApplyActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
    }
    }

   private void createDroppingList () {

        Spinner dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);

        String[] items = new String[] { "5", "4", "3", "2" };

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item,items);

        dynamicSpinner.setAdapter(adapter2);

        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch (position) {
                    case 0:
                        uspijeh = "odličnim";
                        break;
                    case 1:
                        uspijeh = "vrlodobrim";
                        break;
                    case 2:
                        uspijeh = "dobrim";
                        break;
                    case 3:
                        uspijeh = "zadovoljavajućim";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


    }
    private void createDroppingList2 () {

        Spinner dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner2);

        String[] items = new String[] { "5", "4", "3", "2" };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,items);

        dynamicSpinner.setAdapter(adapter);

        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch (position) {
                    case 0:
                        vladanje = "primjernim";
                        break;
                    case 1:
                        vladanje = "vrlodobrim";
                        break;
                    case 2:
                        vladanje = "dobrim";
                        break;
                    case 3:
                        vladanje = "zadovoljavajućim";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


    }

    public void checkTheInput(EditText a, EditText b, EditText c, EditText d, EditText e, TextView f, TextView g, RadioButton but1, RadioButton but2, RadioButton but3, RadioButton but4){

            if (a.getText().toString().equalsIgnoreCase("")){
                a.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
            }
        if (b.getText().toString().equalsIgnoreCase("")){
            b.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
        }
        if (c.getText().toString().equalsIgnoreCase("")){
            c.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
        }
        if (d.getText().toString().equalsIgnoreCase("")){
            d.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
        }
        if (e.getText().toString().equalsIgnoreCase("")){
            e.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
        }
        if (!but1.isChecked() && !but2.isChecked()){
            f.setTextColor(Color.parseColor("#F90808"));
        }
        if (!but3.isChecked() && !but4.isChecked()){
            g.setTextColor(Color.parseColor("#F90808"));

        }






    }



    public void checkBoxes() {

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.spolgrupa);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.muško) {
                    spol = "M";
                } else if(checkedId == R.id.žensko) {
                    spol = "Ž";
                }
            }

        });

    }

    public void checkBoxes2() {

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.ŠkolaGrupa);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.student) {
                    zelimBiti = "student";
                } else if(checkedId == R.id.srednjoskolac) {
                    zelimBiti = "srednjoškolac";
                }

            }

        });



    }
}
