package ba.izvornade.izvornade;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Hasan on 09.04.2018..
 */

public class Scrapper extends IntentService {



    public Scrapper() {
        super("Scrapper");

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //JSoup
        final String TAG = "AQ";

        Document doc = null;
        try {
            doc = Jsoup.connect("http://en.wikipedia.org/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, doc.title());
        Elements newsHeadlines = doc.select("#mp-itn b a");
        for (Element headline : newsHeadlines) {
            Log.i(TAG,"%s\n\t%s" +
                    headline.attr("title") + headline.absUrl("href"));

        }
        // I want to put the title in the TextView in activity_log_in.xml
        //wiki.setText(doc.title());

    }
}
