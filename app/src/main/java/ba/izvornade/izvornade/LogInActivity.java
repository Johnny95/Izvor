package ba.izvornade.izvornade;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class LogInActivity extends AppCompatActivity {

    //Builder used for notifications
    NotificationCompat.Builder notification;
    //every notification has to have an ID, so that the phone can differentiate among them
    private static final int uniqueID = 45612;

    //Database
    Statement myStmt= null;
    Connection myconn=null;
    ResultSet myRs=null;
    TextView sql;
    String s;
    Button sqlB, notButton;

    //popupWindow

    private PopupWindow popupWindow;
    private LayoutInflater layoutInflater;
    private RelativeLayout relativeLayout;
    private Button popButton;
    TextView wiki;
    private ImageView jSoupImage;
    private TextView jSoupText;
    String [] article1 = new String[6];

    //Bitmap[] bitmaps = new Bitmap[6];

    //pager
    ViewPager mPager2;
    loadSomeStuff.JSoupPagerAdapter mPagerAdapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        notification = new NotificationCompat.Builder(this);
        //Deletes the notification when user clicks on it
        notification.setAutoCancel(true);
        sql= (TextView)findViewById(R.id.sqlText);

        popButton = (Button) findViewById(R.id.popButton);
        popButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LogInActivity.this, DrawerActivity.class);
                i.putExtra("keyForThisData", true);
                startActivity(i);
            }
        });



        notButton= (Button) findViewById(R.id.button2);
        notButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                informButtonClicked(view);
            }
        });


        sqlB = (Button) findViewById(R.id.button3);
        sqlB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    myconn = DriverManager.getConnection("jdbc:mysql://192.168.88.211:3306/baza?useSSL=false", "root", "root");
                    myStmt = myconn.createStatement();
                    myRs = myStmt.executeQuery("SELECT * FROM customers");



                    while(myRs.next()){
                        s= myRs.getString("name");
                        sql.setText(s);

                    }






                }catch (Exception exc){
                    exc.printStackTrace();
                } finally {
                    try { myRs.close(); } catch (Exception e) { /* ignored */ }
                    try { myStmt.close(); } catch (Exception e) { /* ignored */ }
                    try { myconn.close(); } catch (Exception e) { /* ignored */ }
                }


            }
        });

      /*  wiki = (TextView) findViewById(R.id.wiki);
        jSoupImage = (ImageView) findViewById(R.id.jSoupImage);
        jSoupText= (TextView) findViewById(R.id.jSoupText);*/
     new loadSomeStuff().execute();
    }

    public void informButtonClicked(View view) {

        //Build the notification
        notification.setSmallIcon(R.drawable.not);
        notification.setTicker("Poruka od direktora");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Title");
        notification.setContentText("Body");


        //doing things when the notification is clicked
        Intent intent= new Intent(this, DrawerActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pIntent);


        //Builds notification and issues it
        NotificationManager nm= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());



    }
 //Not used
    public class loadSomeStuff extends AsyncTask<String, Document, String[]> {


     @Override
        protected String[] doInBackground(String... strings) {
            //JSoup
            final String TAG = "AQ";

            List<String> ids=null;

            Document doc = null;
            try {
                doc = Jsoup.connect("http://izvornade.com/category/aktuelno/").get();
            } catch (IOException e) {
                e.printStackTrace();
            }



           //First item
         Element masthead = doc.select("div.article-container").first();
         Document doc1 = Jsoup.parse(masthead.toString());
         Element eimg1 = doc1.select("img").first();
         String img1 = eimg1.attr("src"); // "http://example.com/"
         Element etext1 = doc1.select("a").first();
         String title1 = etext1.attr("title"); // "http://example.com/"


         article1 [0] = img1;
         article1 [1] = title1;





         //Second item
         Elements masthead2 = doc.select("div.article-container article + article");
         Document doc2 = Jsoup.parse(masthead2.toString());
         Element eimg2 = doc2.select("img").first();
         String img2 = eimg2.attr("src"); // "http://example.com/"
         Element etext2 = doc2.select("a").first();
         String title2 = etext2.attr("title"); // "http://example.com/"

         article1 [2] = img2;
         article1 [3] = title2;

         //Second item
         Elements masthead3 = doc.select("div.article-container article + article + article");
         Document doc3 = Jsoup.parse(masthead3.toString());
         Element eimg3 = doc3.select("img").first();
         String img3 = eimg3.attr("src"); // "http://example.com/"
         Element etext3 = doc3.select("a").first();
         String title3 = etext3.attr("title"); // "http://example.com/"


         article1 [4] = img3;
         article1 [5] = title3;

         return article1;

         //TODO Umjesto ovoga nakon st imas articles napravi listu bitmapova pa ucitaj asinhrono preko Picasso, pa returnaj njih, te nakon toga ce u onPostu (kada napravis lokalnu varijablu) moci u konstruktor proslijediti listu

        }
     @Override
     protected void onPostExecute(String[] s) {
         super.onPostExecute(s);


         //TODO nakon sto proradi globalno, ovdje proslijedi Bitmap[] b umjesto string, napravi konstruktor za adapter te mu posalji array
         runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 mPager2 = (ViewPager) findViewById(R.id.jSpager);
                 mPagerAdapter2 = new JSoupPagerAdapter(LogInActivity.this);
                 mPager2.setAdapter(mPagerAdapter2);

             }
         });

       /*Picasso.with(LogInActivity.this).load(s[4]).into(jSoupImage);
        jSoupText.setText(s[5]);


         */
     }
     public class JSoupPagerAdapter extends PagerAdapter {



         private Context context;
         private LayoutInflater layoutInflater;
         private RequestCreator[] images = { Picasso.with(LogInActivity.this).load(article1[0]), Picasso.with(LogInActivity.this).load(article1[2])}; //TODO move to doInBackground stuff

         public JSoupPagerAdapter(Context context) {
             this.context = context;
             //TODO proslijedi lokalno umjesto globalno (nakon sto proradi)
         }

         @Override
         public int getCount() {
             return images.length;
         } //TODO bitmaps.lengts

         @Override
         public boolean isViewFromObject(View view, Object object) {
             return view == object;
         }

         @Override
         public Object instantiateItem(ViewGroup container, final int position) {

             layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View view = layoutInflater.inflate(R.layout.custom_layout, null);
             final ImageView imageView = (ImageView) view.findViewById(R.id.imageView2);

            //TODO Setuj lokalnu bitmap umjesto da cekas Picasso.Get(), pa obrisi CIJELI async task
             //imageView.setImageBitmap(bitmaps[position]);

             AsyncTask.execute(new Runnable() {
                 @Override
                 public void run() {
                     //TODO ovo pada je imageview ne postoji u ovom bloku (threadu)
                     /*try {
                         imageView.setImageBitmap(images[position].get());
                     } catch (IOException e) {
                         e.printStackTrace();
                     }*/
                 }
             });



             ViewPager vp = (ViewPager) container;
             vp.addView(view, 0);
             return view;
         }

         @Override
         public void destroyItem(ViewGroup container, int position, Object object) {
             ViewPager vp = (ViewPager) container;
             View view = (View) object;
             vp.removeView(view);

         }
     }

    }



}

